/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Long Hoang
 */
public class hashMap {

    public static void main(String[] args) {
        HashMap<Integer, String> hashmap = new HashMap<>();
        hashmap.put(0, "Java");
        hashmap.put(1, "Kotlin");
        hashmap.put(2, "JavaScript");
        hashmap.put(3, "C++");
        hashmap.put(4, "PHP");
        Set<Map.Entry<Integer, String>> setEntry = hashmap.entrySet();
        System.out.println("Cac entry trong hashmap: " + setEntry);
        Iterator<Map.Entry<Integer, String>> iterator = hashmap.entrySet().iterator();
        System.out.println("Lay cac entry bang cach su dung iterator: ");
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("Ngon ngu co ma la 1: " + hashmap.get(1));
        if (hashmap.containsValue("Java")) {
            System.out.println("Trong hashmap co value Java");
        }
        if (hashmap.containsKey(4)) {
            System.out.println("Trong hashmap co key 4");
        }

        System.out.println("Cac value trong hashmap: ");
        for (String value : hashmap.values()) {
            System.out.println(value);
        }

        System.out.println("Cac key trong hashmap: ");
        for (int key : hashmap.keySet()) {
            System.out.println(key);
        }
        hashmap.remove(2);
        System.out.println("hashmap sau khi remove key 2: " + hashmap);
        hashmap.replace(1, "C#");
        System.out.println("hashmap sau khi replace value Kotlin: " + hashmap);
    }
}
