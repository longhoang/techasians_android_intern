/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Collection;

import java.util.ArrayList;

/**
 *
 * @author Long Hoang
 */
public class arrayList {

    public static void main(String[] args) {
        ArrayList<String> arraylist = new ArrayList();
        arraylist.add("Java");
        arraylist.add("C#");
        arraylist.add("C++");
        arraylist.add("Kotlin");
        arraylist.add(1, "Swift");
        arraylist.add("React Native");
        arraylist.add("Kotlin");
        System.out.println("Kich thuoc mang: " + arraylist.size());
        System.out.println("Noi dung mang: " + arraylist);
        System.out.println("Dung vong lap for cai tien de in mang: ");
        for (String str : arraylist) {
            System.out.println(str);
        }
        System.out.println("Phan tu tai vi tri 3: " + arraylist.get(3));
        System.out.println("Vi tri dau tien cua phan tu Kotlin: " + arraylist.indexOf("Kotlin"));
        System.out.println("Vi tri cuoi cung cua phan tu Kotlin: " + arraylist.lastIndexOf("Kotlin"));
        arraylist.set(1, "PHP");
        System.out.println("Mang sau khi cap nhat lai gia tri tai vi tri 1 thanh PHP: " + arraylist);
        arraylist.remove(2);
        System.out.println("Kich thuoc sau khi xoa phan tu vi tri thu 2: " + arraylist.size());
        arraylist.remove("C++");
        System.out.println("Noi dung mang sau khi xoa phan tu C++: " + arraylist);
    }
}
