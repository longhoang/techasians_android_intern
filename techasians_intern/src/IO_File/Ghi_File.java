/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO_File;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Long Hoang
 */
public class Ghi_File {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        File file;
        String mycontent = "Test ghi file";
        try {

            file = new File("D:\\newfile.txt");
            fos = new FileOutputStream(file);

            if (!file.exists()) {
                file.createNewFile();
            }

            byte[] bytesArray = mycontent.getBytes();

            fos.write(bytesArray);
            System.out.println("Da ghi thanh cong");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                System.out.println("Loi dong file");
            }
        }
    }
}
