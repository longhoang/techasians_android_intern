/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO_File;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author Long Hoang
 */
public class Doc_File {

    public static void main(String[] args) {
        File file = new File("D:\\newfile.txt");
        BufferedInputStream bis = null;
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(file);

            bis = new BufferedInputStream(fis);

            while (bis.available() > 0) {
                System.out.print((char) bis.read());
            }

        } catch (IOException e) {
            System.out.println("Da co loi: " + e);
        } finally {
            try {
                if (bis != null && fis != null) {
                    fis.close();
                    bis.close();
                }
            } catch (IOException e) {
                System.out.println("Da xay ra loi: " + e);
            }
        }
    }
}
