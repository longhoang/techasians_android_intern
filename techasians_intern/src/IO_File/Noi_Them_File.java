/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO_File;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Long Hoang
 */
public class Noi_Them_File {

    public static void main(String[] args) {
        try {
            File file = new File("D:\\newfile.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            pw.println("");
            pw.println("Monday");
            pw.println("Tuesday");
            pw.println("Wednesday");
            pw.close();

            System.out.println("Da ghi them file thanh cong");

        } catch (IOException ioe) {
            System.out.println("Da co loi xay ra");
            ioe.printStackTrace();
        }
    }
}
