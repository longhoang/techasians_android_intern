/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Try_catch;

/**
 *
 * @author Long Hoang
 */
public class try_catch_finally {

    public static void main(String[] args) {
        int a, b;
        try {
            a = 0;
            b = 23 / a;
            System.out.println("Ket qua khi chia 23 cho 0: " + b);
        } catch (Exception e) {
            System.out.println("Da xay ra loi");
        } finally {
            System.out.println("Finally chay du cho Catch co chay hay khong ");
        }
    }
}
