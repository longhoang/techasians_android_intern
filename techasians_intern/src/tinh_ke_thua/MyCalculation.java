/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tinh_ke_thua;

/**
 *
 * @author Long Hoang
 */
public class MyCalculation extends Calculation {

    public void phepNhan(int a, int b) {
        c = a * b;
        System.out.println("Tích 2 số = " + c);
    }

    public void phepLuyThua(int a, int b) {
        c = (int) Math.pow(a, b);
        System.out.println(a + "^" + b + " = " + c);
    }

    public static void main(String[] args) {
        int a = 12, b = 2;
        MyCalculation myCalculation = new MyCalculation();
        myCalculation.phepCong(a, b);
        myCalculation.phepTru(a, b);
        myCalculation.phepNhan(a, b);
        myCalculation.phepLuyThua(a, b);
    }
}
