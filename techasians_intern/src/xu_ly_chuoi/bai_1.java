package xu_ly_chuoi;

import java.util.Scanner;

//Viết chương trình nhập từ bàn phím một chuỗi không quá 80 ký tự và một ký tự bất kỳ.
//Đếm và in ra màn hình số lần xuất hiện của ký tự đó trong chuỗi vừa nhập
public class bai_1 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String chuoi;
        char kytu;
        int count = 0;

        do {
            System.out.println("Nhap vao chuoi bat ky: ");
            chuoi = scanner.nextLine();
        } while (chuoi.length() > 80);

        System.out.println("Nhap vao ky tu can dem: ");
        kytu = scanner.next().charAt(0);

        for (int i = 0; i < chuoi.length(); i++) {
            if (kytu == chuoi.charAt(i)) {
                count++;
            }
        }

        System.out.println("So lan xuat hien cua ky tu " + kytu + "la: " + count);
    }
}
