package xu_ly_chuoi;

import java.util.Scanner;

//Viết chương trình nhập vào một chuỗi bất kỳ bao gồm cả số, ký tự thường và ký tự hoa từ bàn phím. 
//Sau đó đếm và in ra số ký tự thường và ký tự hoa và số có trong chuỗi đó
public class bai_2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String chuoi;
        int thuong = 0, hoa = 0, so = 0;
        do {
            System.out.println("Nhap vao chuoi bat ky: ");
            chuoi = scanner.nextLine();
        } while (chuoi.length() > 80);
        for (int i = 0; i < chuoi.length(); i++) {
            if (Character.isUpperCase(chuoi.charAt(i))) {
                hoa++;
            }
            if (Character.isLowerCase(chuoi.charAt(i))) {
                thuong++;
            }
            if (Character.isDigit(chuoi.charAt(i))) {
                so++;
            }
        }
        System.out.println("So ky tu hoa: " + hoa + ", "
                + "So ky tu thuong: " + thuong + ", "
                + "So luong so: " + so);
    }
}
